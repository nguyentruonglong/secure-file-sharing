﻿using DemoAss1.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DemoAss1.Common
{
    public class Converter
    {
        public static Guid StringToGuid(string value)
        {
            byte[] data = MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(value));
            return new Guid(data);
        }

        // Convert an object to a byte array
        public static byte[] RSAParametersToByteArray(RSAParameters obj)
        {
            var objj = new RSAParameter(obj);
            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream ms = new MemoryStream();
            bf.Serialize(ms, objj);

            return ms.ToArray();
        }

        public static byte[] ObjectToByteArray(Object obj)
        {
            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream ms = new MemoryStream();
            bf.Serialize(ms, obj);

            return ms.ToArray();
        }

        public static Object ByteArrayToObject(byte[] byteArray)
        {
            MemoryStream memStream = new MemoryStream();
            BinaryFormatter binForm = new BinaryFormatter();
            memStream.Write(byteArray, 0, byteArray.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            Object obj = (Object)binForm.Deserialize(memStream);

            return obj;
        }

        // Convert a byte array to an Object
        public static RSAParameters ByteArrayToRSAParameters(byte[] arrBytes)
        {
            MemoryStream memStream = new MemoryStream();
            BinaryFormatter binForm = new BinaryFormatter();
            memStream.Write(arrBytes, 0, arrBytes.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            RSAParameter obj = (RSAParameter)binForm.Deserialize(memStream);

            var result = new RSAParameters();
            result.D = obj.D;
            result.DP = obj.DP;
            result.DQ = obj.DQ;
            result.Exponent = obj.Exponent;
            result.InverseQ = obj.InverseQ;
            result.Modulus = obj.Modulus;
            result.P = obj.P;
            result.Q = obj.Q;

            return result;
        }

        public static byte[] SplitByteArray(byte[] bytes, int offset)
        {
            byte[] buffer = new byte[offset];
            using (var stream = new MemoryStream(bytes))
            {
                stream.Read(buffer, 0, offset);

            }
            return buffer;
        }

        public static byte[] TrimByteArray64bit(byte[] bytes, int offset)
        {
            
            int mod = offset % 8;
            int length = mod == 0 ? offset : offset + (8 - mod);
            byte[] buffer = new byte[length];
            using(var stream = new MemoryStream(bytes))
            {
                stream.Read(buffer, 0, offset);
                
            }
            return buffer;
        }

        public static byte[] TrimEnd(byte[] array, byte x)
        {
            byte[] buffer = new byte[array.Length - x];
            using (var stream = new MemoryStream(array))
            {
                stream.Read(buffer, 0, buffer.Length);

            }
            return buffer;
        }

        public static byte[] Trim(byte[] array)
        {

            int i = 0;
            for (i = array.Length - 1; i >= 0; i--)
            {
                if (array[i] != 0)
                    break;
                //memStream.WriteByte(array[i]);
            }
            byte[] bytes = new byte[i + 1];
            using (var mStream = new MemoryStream(array))
            {
                mStream.Read(bytes, 0, bytes.Length);
            }
            return bytes;

        }

        public static string FileToMd5(string filePath)
        {
            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(filePath))
                {
                    return Encoding.Default.GetString(md5.ComputeHash(stream));
                }
            }
        }
    }
}
