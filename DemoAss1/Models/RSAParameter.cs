﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DemoAss1.Models
{
    [Serializable]
    public class RSAParameter
    {
        public byte[] D;
        public byte[] DP;
        public byte[] DQ;
        public byte[] Exponent;
        public byte[] InverseQ;
        public byte[] Modulus;
        public byte[] P;
        public byte[] Q;


        public RSAParameter(RSAParameters key)
        {
            D = key.D;
            DP = key.DP;
            DQ = key.DQ;
            Exponent = key.Exponent;
            InverseQ = key.InverseQ;
            Modulus = key.Modulus;
            P = key.P;
            Q = key.Q;
        }
    }
}
