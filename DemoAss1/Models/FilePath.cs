﻿using DemoAss1.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DemoAss1.Models
{
    public class FilePath : INotifyPropertyChanged
    {
        private String _path;
        public ICommand RemoveCommand { get; set; }

        public FilePath()
        {
            _path = String.Empty;
        }
        public FilePath(String path)
        {
            _path = path;
        }

        private void OnRemoveCommand()
        {
            
        }

        public String Path
        {
            get { return _path; }
            set
            {
                _path = value;
                OnPropertyChanged("Path");
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        // Create the OnPropertyChanged method to raise the event
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

    }
    
}
