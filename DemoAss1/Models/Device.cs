﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoAss1.Models
{
    public class Device : INotifyPropertyChanged
    {
        public Device()
        {

        }

        private String _hostName;

        public String HostName
        {
            get { return _hostName; }
            set
            {
                _hostName = value;
                OnPropertyChanged("HostName");
            }
        }

        private String _ip;

        public String Ip
        {
            get { return _ip; }
            set
            {
                _ip = value;
                OnPropertyChanged("Ip");
            }
        }


        private String _macAddr;

        public String MacAddr
        {
            get { return _macAddr; }
            set
            {
                _macAddr = value;
                OnPropertyChanged("MacAddr");
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        // Create the OnPropertyChanged method to raise the event
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
