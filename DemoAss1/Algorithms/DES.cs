﻿using DemoAss1.Common;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace DemoAss1.Algorithms
{
    public class DES : ProgressEvent
    {
        public byte[] GenerateKey()
        {
            var des = new DESCryptoServiceProvider();
            des.GenerateKey();
            return des.Key;
        }

        public void Execute(bool isEncrypt, string filePath, byte[] key)
        {
            
            OnStarting();
            int offset = -1;
            byte[] bytes = new byte[1024 * 10];
            string root = Path.GetDirectoryName(filePath);
            try
            {
                if (isEncrypt)
                {
                    using(var outStream = File.Open(filePath.Split('.')[0] + ".encrypt",
                        FileMode.Create, FileAccess.ReadWrite))
                    {

                        int xOffset = 0;
                        using (var inStream = File.OpenRead(filePath))
                        {
                            long total = 0;
                            outStream.Write(new byte[] { 0 }, 0, 1);
                            while ((offset = inStream.Read(bytes, 0, bytes.Length)) > 0)
                            {
                                if(offset < bytes.Length)
                                {
                                    bytes = Converter.TrimByteArray64bit(bytes, offset);
                                    xOffset = offset;
                                }
                                var ct = Encrypt(bytes, key, key);
                                outStream.Write(ct, 0, ct.Length);
                                outStream.Flush();

                                total += offset;
                                OnProgress(total * 100 / inStream.Length);
                            }
                        }
                        outStream.Seek(0, SeekOrigin.Begin);
                        byte x = (byte)((xOffset % 8) == 0 ? 0 : 8 - (xOffset % 8));
                        outStream.Write(new byte[] { x }, 0, 1);
                        outStream.Flush();
                    }
                }
                else
                {
                    using (FileStream outStream = File.Open(filePath.Split('.')[0] + ".decrypt",
                        FileMode.Create, FileAccess.ReadWrite))
                    {
                        using (FileStream inStream = File.OpenRead(filePath))
                        {
                            long total = 0;
                            byte[] x = new byte[1];
                            inStream.Read(x, 0, x.Length);
                            while ((offset = inStream.Read(bytes, 0, bytes.Length)) > 0)
                            {
                                byte[] pt = new byte[0];
                                if (offset < bytes.Length)
                                {
                                    bytes = Converter.TrimByteArray64bit(bytes, offset);
                                    pt = Decrypt(bytes, key, key);
                                    pt = Converter.TrimEnd(pt, x[0]);
                                }
                                else
                                {
                                    pt = Decrypt(bytes, key, key);
                                }
                                
                                outStream.Write(pt, 0, pt.Length);
                                outStream.Flush();

                                total += offset;
                                OnProgress(total * 100 / inStream.Length);
                            }
                        }
                    }
                }
                OnCompleted();
            }
            catch(Exception ex)
            {
                OnError();
            }
        }

        public byte[] Encrypt(byte[] data, byte[] key, byte[] iv)
        {
            DESCryptoServiceProvider cryptoProvider = new DESCryptoServiceProvider() { Padding = PaddingMode.None};
            using (MemoryStream memoryStream = new MemoryStream())
            {
                using(var transform = cryptoProvider.CreateEncryptor(key, iv))
                {
                    using (CryptoStream cryptoStream = new CryptoStream(
                            memoryStream, transform, CryptoStreamMode.Write))
                    {
                        cryptoStream.Write(data, 0, data.Length);
                    }
                }
                return memoryStream.ToArray();
            }
        }


        public byte[] Decrypt(byte[] data, byte[] key, byte[] iv)
        {
            DESCryptoServiceProvider cryptoProvider = new DESCryptoServiceProvider() { Padding = PaddingMode.None };
            using (MemoryStream memoryStream = new MemoryStream(data))
            {
                using (var transform = cryptoProvider.CreateDecryptor(key, iv))
                {
                    using (CryptoStream cryptoStream = new CryptoStream(
                                memoryStream, transform, CryptoStreamMode.Read))
                    {
                        using (MemoryStream ms = new MemoryStream())
                        {
                            cryptoStream.CopyTo(ms);
                            return ms.ToArray();
                        }
                    }
                }
            }
        }

        
    }
}
