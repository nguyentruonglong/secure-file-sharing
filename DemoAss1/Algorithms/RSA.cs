﻿using DemoAss1.Common;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace DemoAss1.Algorithms
{
    public class RSA : ProgressEvent
    {
        //string EncryptFile = @"C:\Users\nguyentruonglong\Desktop\EncryptFile.dat";
        
        RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
        
        /// <summary>
        /// if decrypt then plainText = null
        /// </summary>
        /// <param name="isEncrypt"></param>
        /// <param name="plainText"></param>
        public byte[] Execute(bool isEncrypt, byte[] rsaKey, byte[] text)
        {
            //using (var stream = File.OpenRead(App.KeyFile))
            {
                if (isEncrypt)
                {

                    var bytes = Encryption(
                        text,
                        Converter.ByteArrayToRSAParameters(rsaKey),
                        false
                    );

                    //File.WriteAllBytes(EncryptFile, bytes);
                    //stream.Close();
                    return bytes;
                    
                }
                else
                {
                    //byte[] priKey = new byte[privateKeyLength];
                    //byte[] data = File.ReadAllBytes(EncryptFile);
                    
                    //stream.Seek(publicKeyLength, SeekOrigin.Current);
                    //stream.Read(priKey, 0, priKey.Length);
                    //stream.Read(data, 0, data.Length);

                    var bytes = Decryption(
                        text,
                        Converter.ByteArrayToRSAParameters(rsaKey),
                        false
                    );

                    //stream.Close();
                    //if (bytes != null)
                    //    File.WriteAllBytes(outputPlainText, bytes);
                    return bytes;
                }
            }
            
        }

        public void GenKey()
        {
            //Save the public key information to an RSAParameters structure.  
            RSAParameters publicKey = rsa.ExportParameters(false);
            var publicKeyBytes = Converter.RSAParametersToByteArray(publicKey);
            
            RSAParameters privateKey = rsa.ExportParameters(true);
            var privateKeyBytes = Converter.RSAParametersToByteArray(privateKey);

            File.WriteAllBytes(App.PublicKeyFile, publicKeyBytes);
            File.WriteAllBytes(App.PrivateKeyFile, privateKeyBytes);

            //using(var stream = File.OpenWrite(App.KeyFile))
            //{
            //    stream.Write(publicKeyBytes, 0, publicKeyBytes.Length);
            //    stream.Write(privateKeyBytes, 0, privateKeyBytes.Length);
            //    stream.Flush();
            //    stream.Close();
            //}
        }


        public byte[] Encryption(byte[] Data, RSAParameters RSAKey, bool DoOAEPPadding)
        {
            try
            {
                byte[] encryptedData;
                //using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
                {
                    rsa.ImportParameters(RSAKey);
                    encryptedData = rsa.Encrypt(Data, DoOAEPPadding);
                }
                return encryptedData;
            }
            catch (CryptographicException e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }

        public byte[] Decryption(byte[] Data, RSAParameters RSAKey, bool DoOAEPPadding)
        {
            try
            {
                byte[] decryptedData;
                //using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
                {
                    rsa.ImportParameters(RSAKey);
                    decryptedData = rsa.Decrypt(Data, DoOAEPPadding);
                }
                return decryptedData;
            }
            catch (CryptographicException e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }
        }
    }
}
