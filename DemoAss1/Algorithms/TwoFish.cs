﻿using DemoAss1.Common;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using TwofishLibrary;

namespace DemoAss1.Algorithms
{
    public class TwoFish : ProgressEvent
    {

        public byte[] GenerateKey()
        {
            var twofish = new TwofishManaged() { KeySize = 256, Mode = CipherMode.ECB, Padding = PaddingMode.None };

            twofish.GenerateKey();
            return twofish.Key;
        }

        public void Execute(bool isEncrypt, string filePath, byte[] key)
        {
            OnStarting();
            int offset = -1;
            byte[] bytes = new byte[1024 * 10];

            //string root = Path.GetDirectoryName(filePath);

            try
            {
                if (isEncrypt)
                {
                    using (FileStream outStream = File.OpenWrite(filePath.Split('.')[0] + ".encrypt"))
                    {
                        int xOffset = 0;
                        using (FileStream inStream = File.OpenRead(filePath))
                        {
                            long total = 0;
                            outStream.Write(new byte[] { 0 }, 0, 1);
                            while ((offset = inStream.Read(bytes, 0, bytes.Length)) > 0)
                            {
                                if (offset < bytes.Length)
                                {
                                    bytes = Converter.TrimByteArray64bit(bytes, offset);
                                    xOffset = offset;
                                }
                                var ct = Encrypt(bytes, key, null);
                                outStream.Write(ct, 0, ct.Length);
                                outStream.Flush();

                                total += offset;
                                OnProgress(total * 100 / inStream.Length);
                            }
                        }
                        outStream.Seek(0, SeekOrigin.Begin);
                        byte x = (byte)((xOffset % 8) == 0 ? 0 : 8 - (xOffset % 8));
                        outStream.Write(new byte[] { x }, 0, 1);
                        outStream.Flush();
                    }
                }
                else
                {
                    using (FileStream outStream = File.OpenWrite(filePath.Split('.')[0] + ".decrypt"))
                    {
                        using (FileStream inStream = File.OpenRead(filePath))
                        {
                            long total = 0;
                            byte[] x = new byte[1];
                            inStream.Read(x, 0, x.Length);
                            while ((offset = inStream.Read(bytes, 0, bytes.Length)) > 0)
                            {
                                byte[] pt = new byte[0];
                                if (offset < bytes.Length)
                                {
                                    bytes = Converter.TrimByteArray64bit(bytes, offset);
                                    pt = Decrypt(bytes, key, key);
                                    pt = Converter.TrimEnd(pt, x[0]);
                                }
                                else
                                {
                                    pt = Decrypt(bytes, key, key);
                                }

                                outStream.Write(pt, 0, pt.Length);
                                outStream.Flush();

                                total += offset;
                                OnProgress(total * 100 / inStream.Length);
                            }
                        }
                    }
                }
                OnCompleted();
            }
            catch(Exception ex)
            {
                OnError();
            }
        }

        private byte[] Encrypt(byte[] pt, byte[] key, byte[] iv)
        {
            var algorithm = new TwofishManaged() { KeySize = 256, Mode = CipherMode.ECB, Padding = PaddingMode.None};
            using (var ms = new MemoryStream())
            {
                using (var transform = algorithm.CreateEncryptor(key, iv))
                {
                    using (var cs = new CryptoStream(ms, transform, CryptoStreamMode.Write))
                    {
                        cs.Write(pt, 0, pt.Length);
                    }
                }
                return ms.ToArray();
            }
        }

        private byte[] Decrypt(byte[] ct, byte[] key, byte[] iv)
        {
            var algorithm = new TwofishManaged() { KeySize = 256, Mode = CipherMode.ECB, Padding = PaddingMode.None };
            using (var ctStream = new MemoryStream(ct))
            {
                using (var transform = algorithm.CreateDecryptor(key, iv))
                {
                    using (var cs = new CryptoStream(ctStream, transform, CryptoStreamMode.Read))
                    {
                        using (var ms = new MemoryStream())
                        {
                            cs.CopyTo(ms);
                            return ms.ToArray();
                        }
                    }
                }
            }
        }

        
    }

}
