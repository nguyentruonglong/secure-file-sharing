﻿namespace DemoAss1.Services
{
    public enum Protocol
    {
        AUTHENTICATION,
        GET_PUBLIC_KEY,
        ENCRYPT_KEY,
        SEND_DATA,
        COMPLETED,
        INVALID
    }

    public enum Status
    {
        OK,
        ERROR
    }
}
