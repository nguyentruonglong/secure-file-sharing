﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InTheHand.Net.Sockets;
using InTheHand.Net;
using DemoAss1.Common;
using System.IO;

namespace DemoAss1.Services
{
    public class SenderBluetoothService
    {
        private readonly Guid _serviceClassId;
        private BluetoothDeviceInfo _deviceClass;

        public SenderBluetoothService(BluetoothDeviceInfo deviceClass)
        {
            _deviceClass = deviceClass;
            // gen key for connect
            _serviceClassId = Converter.StringToGuid(_deviceClass.DeviceAddress.ToString());
        }

        /// <summary>
        /// if isFile == true then value is FilePath else if isFile == false then value is Text
        /// </summary>
        /// <param name="value">FilePpath or Text</param>
        /// <param name="isFile">true or false</param>
        /// <returns></returns>
        public async Task<bool> Send(String value, bool isFile)
        {
            OnConnecting();
            if (_deviceClass == null)
            {
                throw new ArgumentNullException("device");
            }

            if (value == null || value.Length == 0)
            {
                throw new ArgumentNullException("content");
            }

            // for not block the UI it will run in a different threat
            var task = Task.Run(() =>
            {
                using (var bluetoothClient = new BluetoothClient())
                {
                    try
                    {
                        var ep = new BluetoothEndPoint(_deviceClass.DeviceAddress, _serviceClassId);

                        // connecting
                        bluetoothClient.Connect(ep);
                        OnStartSend();

                        // get stream for send the data
                        var bluetoothStream = bluetoothClient.GetStream();

                        // if all is ok to send
                        if (bluetoothClient.Connected && bluetoothStream != null)
                        {
                            Stream stream = null;
                            if (!isFile)
                                // Text Stream
                                stream = new MemoryStream(Encoding.UTF8.GetBytes(value));
                            else
                                // File Stream
                                stream = File.OpenRead(value);

                            // Send data
                            byte[] bytes = new byte[2048];
                            int offset = -1;
                            while ((offset = stream.Read(bytes, 0, bytes.Length)) > 0)
                            {
                                bluetoothStream.Write(bytes, 0, offset);
                                bluetoothStream.Flush();
                                OnProgressing();
                            }

                            bluetoothStream.Close();
                            OnCompleted();
                            return true;
                        }
                        OnCompleted();
                        return false;
                    }
                    catch (Exception ex)
                    {
                        OnError();
                        // the error will be ignored and the send data will report as not sent
                        // for understood the type of the error, handle the exception
                    }
                }
                return false;
            });
            return await task;
        }
        
        public event EventHandler Connecting;
        private void OnConnecting()
        {
            Connecting?.Invoke(this, EventArgs.Empty);
        }

        public event EventHandler StartSend;
        private void OnStartSend()
        {
            StartSend?.Invoke(this, EventArgs.Empty);
        }
        public event EventHandler Completed;
        private void OnCompleted()
        {
            Completed?.Invoke(this, EventArgs.Empty);
        }

        public event EventHandler Progressing;
        private void OnProgressing()
        {
            Progressing?.Invoke(this, EventArgs.Empty);
        }

        public event EventHandler Error;
        private void OnError()
        {
            Error?.Invoke(this, EventArgs.Empty);
        }
    }
}
