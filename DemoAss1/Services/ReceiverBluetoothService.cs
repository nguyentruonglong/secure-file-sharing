﻿using InTheHand.Net.Sockets;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DemoAss1.Services
{
    public class ReceiverBluetoothService
    {
        private readonly Guid _serviceClassId;
        private Action<string> _responseAction;
        private BluetoothListener _listener;
        private CancellationTokenSource _cancelSource;
        private bool _wasStarted;
        private bool _status;

        public ReceiverBluetoothService()
        {
            var id = GetLocalBluetoothAddress();
            if (id != null)
            {
                _serviceClassId = new Guid(id.ToString());
            }
        }

        public bool WasStarted
        {
            get { return _wasStarted; }
            set { _wasStarted = value; }
        }

        private PhysicalAddress GetLocalBluetoothAddress()
        {
            //Get list local network card
            var networkList = NetworkInterface.GetAllNetworkInterfaces();
            foreach (NetworkInterface nic in networkList)
            {
                if (nic.NetworkInterfaceType != NetworkInterfaceType.Ethernet) continue;

                // Get bluetooth card info
                if (nic.Name.Contains("Bluetooth"))
                {
                    return nic.GetPhysicalAddress();
                }
            }
            return null;
        }

        // Starts the listening from Senders.
        public void Start(Action<string> reportAction)
        {
            OnListening();
            WasStarted = true;
            _responseAction = reportAction;
            if (_cancelSource != null && _listener != null)
            {
                Dispose(true);
            }
            _listener = new BluetoothListener(_serviceClassId)
            {
                ServiceName = "MyService"
            };
            _listener.Start();

            _cancelSource = new CancellationTokenSource();

            Task.Run(() => Listener(_cancelSource));
        }
        
        // Stops the listening from Senders.
        public void Stop()
        {
            WasStarted = false;
            _cancelSource.Cancel();
        }
        // Listeners the accept bluetooth client.
        private void Listener(CancellationTokenSource token)
        {
            try
            {
                while (true)
                {
                    using (var client = _listener.AcceptBluetoothClient())
                    {
                        OnStartReceive();
                        if (token.IsCancellationRequested)
                        {
                            return;
                        }

                        using (var bluetoothStream = client.GetStream())
                        {
                            try
                            {
                                var fileName = new Guid().ToString();
                                string filePath = GetTempFilePathWithExtension(fileName);
                                using(var stream = File.OpenRead(filePath))
                                {
                                    int offset = -1;
                                    byte[] bytes = new byte[2048];
                                    while ((offset = bluetoothStream.Read(bytes, 0, bytes.Length)) > 0)
                                    {
                                        stream.Write(bytes, 0, offset);
                                        stream.Flush();
                                        OnProgress();
                                    }
                                    stream.Close();
                                }

                                OnCompleted();
                            }
                            catch (IOException)
                            {
                                OnError();
                                client.Close();
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                OnError();
                // todo handle the exception
                // for the sample it will be ignored
            }
        }

        public string GetTempFilePathWithExtension(string filename)
        {
            var folderPath = Path.GetTempPath();
            return Path.Combine(folderPath, filename + ".tmp");
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// The dispose.
        /// </summary>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_cancelSource != null)
                {
                    _listener.Stop();
                    _listener = null;
                    _cancelSource.Dispose();
                    _cancelSource = null;
                }
            }
        }

        public event EventHandler Listening;
        private void OnListening()
        {
            Listening?.Invoke(this, EventArgs.Empty);
        }

        public event EventHandler StartReceive;
        private void OnStartReceive()
        {
            StartReceive?.Invoke(this, EventArgs.Empty);
        }

        public event EventHandler Completed;
        private void OnCompleted()
        {
            Completed?.Invoke(this, EventArgs.Empty);
        }

        public event EventHandler Progress;
        private void OnProgress()
        {
            Progress?.Invoke(this, EventArgs.Empty);
        }

        public event EventHandler Error;
        private void OnError()
        {
            Error?.Invoke(this, EventArgs.Empty);
        }
    }
}
