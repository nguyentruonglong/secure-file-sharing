﻿using DemoAss1.Common;
using System;
using System.IO;
using System.Net.Sockets;

namespace DemoAss1.Services
{
    public class Server : ProgressEvent
    {
        public static int PORT = 8888;
        public static string IP = "localhost";
        private TcpListener _listener;
        private Stream _stream;
        private TcpClient _client;

        public Server()
        {
            _listener = new TcpListener(PORT);
        }
        public void StartService()
        {
            try
            {
                StopService();
                _listener.Start();
                _client = _listener.AcceptTcpClient();
                _stream = _client.GetStream();
            }
            catch(Exception ex)
            {
                OnError();
            }
            
        }

        
        public Protocol ListenFromStream(ref byte[] dataReceived)
        {
            using(MemoryStream memStream = new MemoryStream())
            {
                byte[] bytes = new byte[1024];
                int offset = -1;
                offset = _stream.Read(bytes, 0, bytes.Length);

                // get data;
                if(bytes[0] == (byte)Protocol.AUTHENTICATION)
                {
                    using(MemoryStream memStr = new MemoryStream(bytes))
                    {
                        memStr.Seek(1, SeekOrigin.Current);
                        memStr.CopyTo(memStream);
                        dataReceived = memStream.ToArray();
                    }
                    return Protocol.AUTHENTICATION;
                }
                else if (bytes[0] == (byte)Protocol.SEND_DATA)
                {
                    OnStarting();
                    using (MemoryStream memStr = new MemoryStream(bytes))
                    {
                        memStr.Seek(1, SeekOrigin.Current);
                        memStr.CopyTo(memStream);
                        dataReceived = memStream.ToArray();
                    }
                    return Protocol.SEND_DATA;
                }
                else if(bytes[0] == (byte)Protocol.COMPLETED)
                {
                    dataReceived = null;
                    return Protocol.COMPLETED;
                }
                else if(bytes[0] == (byte)Protocol.ENCRYPT_KEY)
                {
                    using (MemoryStream memStr = new MemoryStream(bytes))
                    {
                        memStr.Seek(1, SeekOrigin.Current);
                        memStr.CopyTo(memStream);
                        dataReceived = memStream.ToArray();
                    }
                    return Protocol.ENCRYPT_KEY;
                }
                else
                {
                    // Invalid protocol
                    dataReceived = null;
                    return Protocol.INVALID;
                }
            }
        }

        public void ResponsePublicKey(byte[] publicKey)
        {
            _stream.Write(publicKey , 0, publicKey.Length);
        }

        /// Return output file path;
        public string ReadFileFromClient(string fileName, string outputFolderPath)
        {
            fileName = fileName.Replace("\0", string.Empty); ;
            var outputFilePath = Path.Combine(outputFolderPath, fileName);
            using (FileStream fStream = File.Open(outputFilePath, FileMode.Create, FileAccess.ReadWrite))
            {
                byte[] bytes = new byte[1024];
                int offset = -1;
                long total = 0;
                try
                {
                    while ((offset = _stream.Read(bytes, 0, bytes.Length)) > 0)
                    {
                        if (offset < bytes.Length)
                        {
                            bytes = Converter.SplitByteArray(bytes, offset);
                            if (bytes[bytes.Length - 1] == (byte)Protocol.COMPLETED)
                            {
                                fStream.Write(bytes, 0, bytes.Length - 1);
                                break;
                            }
                        }
                        fStream.Write(bytes, 0, offset);
                        total += offset;
                        OnProgress(total);
                        //GC.Collect();
                    }
                }
                catch(Exception ex)
                {
                    OnError();
                    return string.Empty;
                }
            }
            OnCompleted();
            return outputFilePath;
        }

        public void ConfirmClientAction(Status status)
        {
            if(_stream != null)
                _stream.WriteByte((byte)status);
        }

        public bool StopService()
        {
            try
            {
                _stream.Dispose();
                _listener.Stop();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
