﻿using DemoAss1.Algorithms;
using DemoAss1.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace DemoAss1.Services
{
    public class Client : ProgressEvent
    {
        private TcpClient _client;
        private Stream _stream;
        private string _serverIp;

        public Client(string serverIp)
        {
            _serverIp = serverIp;
        }

        public bool ConnectServer()
        {
            try
            {
                _client = new TcpClient(_serverIp, Server.PORT);
                _stream = _client.GetStream();
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// send: authen server by computer name
        /// get: public key RSA
        /// </summary>
        /// <returns></returns>
        public byte[] AuthenServer()
        {
            try
            {
                byte[] procol = new byte[] { (byte)Protocol.AUTHENTICATION };
                byte[] computerName = Encoding.ASCII.GetBytes(Environment.MachineName);
                using (MemoryStream memStream = new MemoryStream())
                {
                    memStream.Write(procol, 0, procol.Length);
                    memStream.Write(computerName, 0, computerName.Length);
                    // Send data
                    var buffer = memStream.ToArray();
                    _stream.Write(buffer, 0, buffer.Length);
                    _stream.Flush();

                    // get public key RSA from server
                    byte[] bytes = new byte[1024];
                    _stream.Read(bytes, 0, bytes.Length);
                    return bytes;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool SendEncryptKey(byte[] encryptKey)
        {
            try
            {
                byte[] procol = new byte[] { (byte)Protocol.ENCRYPT_KEY };
                using (MemoryStream memStream = new MemoryStream())
                {
                    memStream.Write(procol, 0, procol.Length);
                    memStream.Write(encryptKey, 0, encryptKey.Length);
                    // Send data
                    var buffer = memStream.ToArray();
                    _stream.Write(buffer, 0, buffer.Length);
                    _stream.Flush();

                    // get response status from server
                    return GetResponseStatusFromServer() == Status.OK ? true : false;
                }
            }
            catch (Exception ex)
            {
                OnError();
                return false;
            }
        }


        /// <summary>
        /// Send: file name
        /// Get: status response from server
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public bool SendFileInfo(string fileName, long fileSize, string algorithmCrypt)
        {
            try
            {
                OnStarting();
                byte[] procol = new byte[] { (byte)Protocol.SEND_DATA };
                //byte[] fileNameBytes = Encoding.ASCII.GetBytes(fileName);
                List<string> objInfo = new List<string>();
                objInfo.Add(fileName);
                objInfo.Add(fileSize.ToString());
                objInfo.Add(algorithmCrypt);
                byte[] fileInfoBytes = Converter.ObjectToByteArray(objInfo);
                // send protocol (the first byte)
                using (MemoryStream memStream = new MemoryStream())
                {
                    memStream.Write(procol, 0, procol.Length);
                    memStream.Write(fileInfoBytes, 0, fileInfoBytes.Length);
                    // Send data
                    var buffer = memStream.ToArray();
                    _stream.Write(buffer, 0, buffer.Length);
                    _stream.Flush();

                    // Get response from server
                    return GetResponseStatusFromServer() == Status.OK ? true : false;
                }
            }
            catch (Exception ex)
            {
                OnError();
                return false;
            }

        }

        /// <summary>
        /// Send: file data
        /// Get: None
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public bool SendFileData(string filePath)
        {
            try
            {
                byte[] bytes = new byte[1024];
                int offset = -1;
                using (var inStream = File.OpenRead(filePath))
                {
                    long total = 0;
                    while ((offset = inStream.Read(bytes, 0, bytes.Length)) > 0)
                    {
                        _stream.Write(bytes, 0, offset);
                        total += offset;
                        OnProgress(total * 100 / inStream.Length);
                        //GC.Collect();
                    }

                    // Complete send file
                    _stream.WriteByte((byte)Protocol.COMPLETED);
                    OnCompleted();
                    return true;
                }
            }
            catch(Exception ex)
            {
                OnError();
                return false;
            }
            
        }
        
        public Status GetResponseStatusFromServer()
        {
            try
            {
                byte[] res = new byte[1];
                _stream.Read(res, 0, res.Length);
                if (res[0] == (byte)Status.OK)
                {
                    return Status.OK;
                }
                return Status.ERROR;
            }
            catch(Exception ex)
            {
                return Status.ERROR;
            }
        }

        public bool StopService()
        {
            try
            {
                _stream.Dispose();
                _client.Close();
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }
    }
}
