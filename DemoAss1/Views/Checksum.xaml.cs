﻿using DemoAss1.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;

namespace DemoAss1.Views
{
    /// <summary>
    /// Interaction logic for Checksum.xaml
    /// </summary>
    public partial class Checksum : Page
    {
        public Checksum()
        {
            InitializeComponent();
            rbtnMD5.IsChecked = true;
        }

        private void button_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            System.Windows.Forms.OpenFileDialog dialog = new System.Windows.Forms.OpenFileDialog();
            dialog.Multiselect = true;
            dialog.Title = "Choose file";
            var result = dialog.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                var path = new FilePath(Path.Combine(dialog.InitialDirectory, dialog.FileName));
                tbxFilePath.Text = path.Path;

                // Hash
                if (rbtnMD5.IsChecked == true)
                {
                    tbxResult.Text = GetMD5HashFromFile(path.Path);
                }
                else if (rbtnSHA1.IsChecked == true)
                {
                    tbxResult.Text = GetSHA1HashFromFile(path.Path);
                }
                else if (rbtnSHA256.IsChecked == true)
                {
                    tbxResult.Text = GetSHA256HashFromFile(path.Path);
                }
            }
        }

        protected string GetMD5HashFromFile(string fileName)
        {
            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(fileName))
                {
                    return BitConverter.ToString(md5.ComputeHash(stream)).Replace("-", string.Empty);
                }
            }
        }


        protected string GetSHA1HashFromFile(string fileName)
        {
            using (var sha1 = SHA1.Create())
            {
                using (var stream = File.OpenRead(fileName))
                {
                    return BitConverter.ToString(sha1.ComputeHash(stream)).Replace("-", string.Empty);
                }
            }
        }

        protected string GetSHA256HashFromFile(string fileName)
        {
            using (var sha256 = SHA256.Create())
            {
                using (var stream = File.OpenRead(fileName))
                {
                    return BitConverter.ToString(sha256.ComputeHash(stream)).Replace("-", string.Empty);
                }
            }
        }
    }
}
