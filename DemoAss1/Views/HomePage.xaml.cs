﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace DemoAss1.Views
{
    /// <summary>
    /// Interaction logic for HomePage.xaml
    /// </summary>
    public partial class HomePage : Page
    {
        public HomePage()
        {
            InitializeComponent();
            
        }

        private void Button_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (App.actionType == ActionType.None)
                App.actionType = ActionType.Receiver;
            
            //Task.Run(()=> 
            //{
            //    App.server.StartService();
                this.NavigationService.Navigate(new ProgressPage());
            //});
            
        }
    }
}
