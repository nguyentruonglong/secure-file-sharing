﻿using DemoAss1.Models;
using DemoAss1.Services;
using InTheHand.Net.Sockets;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace DemoAss1.Views
{
    /// <summary>
    /// Interaction logic for ScanDevicePage.xaml
    /// </summary>
    public partial class ScanDevicePage : Page, INotifyPropertyChanged
    {
        private ObservableCollection<Device> _vLanDeviceList;

        public ScanDevicePage()
        {
            InitializeComponent();
            this.DataContext = this;
            _vLanDeviceList = new ObservableCollection<Device>();
            MessageNotify.Visibility = Visibility.Collapsed;
            ScanBar.Visibility = Visibility.Visible;
        }

        public ObservableCollection<Device> VlanDeviceList
        {
            get
            {
                return _vLanDeviceList;
            }
            set
            {
                _vLanDeviceList = value;
                OnPropertyChanged("VlanDeviceList");
                if(_vLanDeviceList == null || _vLanDeviceList.Count == 0)
                {
                    Application.Current.Dispatcher.BeginInvoke(
                        System.Windows.Threading.DispatcherPriority.Normal, new Action(() =>
                        {
                            MessageNotify.Visibility = Visibility.Visible;
                            ScanBar.Visibility = Visibility.Collapsed;
                        }));
                }
            }
        }

        //private BluetoothDeviceInfo[] ScanBluetoothDevice()
        //{
        //    try
        //    {
        //        BluetoothClient bc = new BluetoothClient();
        //        BluetoothDeviceInfo[] devices = bc.DiscoverDevices();
        //        bc.Dispose();
        //        return devices;
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show("Please turn on Bluetooth on this device!");
        //        return null;
        //    }
        //}

        private ObservableCollection<Device> ScanLanDevice()
        {
            try
            {
                LocalNetwork.Ping_all();
                return LocalNetwork.listNet;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private void Rescan_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
           
        }
        
        private void StopConnect_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ConnectDialog.Visibility = Visibility.Collapsed;
        }
        
        private void lvDevice_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //ConnectDialog.Visibility = Visibility.Visible;

            var selectedDevice = (sender as ListView).SelectedItem as Device;
            if(selectedDevice != null)
            {
                this.NavigationService.Navigate(new AddFilePage(selectedDevice));
                Server.IP = selectedDevice.Ip;
            }
                
            (sender as ListView).SelectedIndex = -1;
        }

        Thread ScanLanThread = null;
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            ScanLanThread = new Thread(new ThreadStart(() =>
            {
                
                    var result = ScanLanDevice();
                    if (result != null)
                    {
                        VlanDeviceList = result;
                    }
                
                
            }));
            ScanLanThread.IsBackground = true;
            ScanLanThread.Start();

        }

        public event PropertyChangedEventHandler PropertyChanged;
        // Create the OnPropertyChanged method to raise the event
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        private void EnterIpRecevier_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            tbIpReceiver.Visibility = Visibility.Visible;
            btnNext.Visibility = Visibility.Visible;
            ScanLanThread.Abort();
            ScanBar.Visibility = Visibility.Collapsed;
        }

        private void btnNext_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var ip = tbIpReceiver.Text.Trim();
            var device = new Device() { Ip = ip, HostName = ip, MacAddr = string.Empty };
            this.NavigationService.Navigate(new AddFilePage(device));
            Server.IP = ip;
        }

        private void Page_Unloaded(object sender, RoutedEventArgs e)
        {
            ScanLanThread.Abort();
        }
    }
    
}
