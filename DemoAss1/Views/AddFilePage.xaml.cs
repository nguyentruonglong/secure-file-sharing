﻿using DemoAss1.Algorithms;
using DemoAss1.Common;
using DemoAss1.Models;
using InTheHand.Net.Sockets;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace DemoAss1.Views
{
    /// <summary>
    /// Interaction logic for AddFilePage.xaml
    /// </summary>
    public partial class AddFilePage : Page, INotifyPropertyChanged
    {
        private ObservableCollection<FilePath> _filePathList;
        private Device _device;
        private string _deviceName;
        private AlgorithmEnum _algorithm;

        /// <summary>
        /// Work for vlan
        /// </summary>
        /// <param name="device"></param>
        public AddFilePage(Device device)
        {
            InitializeComponent();
            this.DataContext = this;
            _device = device;
            _filePathList = new ObservableCollection<FilePath>();
            addFileType.Text = "send";
            radioButton.IsChecked = true;
        }
        

        /// <summary>
        /// Encrypt/Decrypt file on local deveice
        /// </summary>
        /// <param name="isEncrypt"></param>
        public AddFilePage()
        {
            InitializeComponent();
            this.DataContext = this;
            _device = null;
            _filePathList = new ObservableCollection<FilePath>();
            if (App.actionType == ActionType.Encryptor)
                addFileType.Text = "encrypt";
            else if(App.actionType == ActionType.Decryptor)
                addFileType.Text = "decrypt";

            radioButton.IsChecked = true;
        }

        public ObservableCollection<FilePath> FilePathList
        {
            get
            {
                return _filePathList;
            }
            set
            {
                _filePathList = value;
                OnPropertyChanged("FilePathList");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }

        private void Border_Drop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                var data = e.Data.GetData(DataFormats.FileDrop);
                AreaDropData.Visibility = Visibility.Collapsed;
                AreaDropData.AllowDrop = false;
                foreach(var item in data as Array)
                {
                    var path = new FilePath(item as string);
                    FilePathList.Add(path);
                    path.RemoveCommand = new DelegateCommand(() =>
                    {
                        FilePathList.Remove(path);
                        if(FilePathList.Count <= 0)
                        {
                            AreaDropData.Visibility = Visibility.Visible;
                            AreaDropData.AllowDrop = true;
                            if (parentGrid.AllowDrop)
                            {
                                parentGrid.AllowDrop = false;
                                parentGrid.Drop -= ParentGrid_Drop;
                            }
                        }
                    }, true);

                }
                if (!parentGrid.AllowDrop)
                {
                    parentGrid.AllowDrop = true;
                    parentGrid.Drop += ParentGrid_Drop;
                }
                
            }
            else if (e.Data.GetDataPresent(DataFormats.Text))
            {
                var data = e.Data.GetData(DataFormats.Text);
                AreaDropData.Visibility = Visibility.Collapsed;
            }
        }
        
        private void ParentGrid_Drop(object sender, DragEventArgs e)
        {
            Border_Drop(sender, e);
        }

        private void BtnNext_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (App.actionType == ActionType.None)
                App.actionType = ActionType.Sender;
            this.NavigationService.Navigate(new ProgressPage(_device, _filePathList, _algorithm));
            
        }

        private void textBlock_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {

        }

        private void radioButton_Checked(object sender, RoutedEventArgs e)
        {
            textBlock1.Text = "CBC mode, 64 bits key";
            textBlock1_Copy.Text = "None padding";
            _algorithm = AlgorithmEnum.DES;
        }

        private void radioButton_Copy_Checked(object sender, RoutedEventArgs e)
        {
            textBlock1.Text = "ECB mode, 256 bits key";
            textBlock1_Copy.Text = "None padding";
            _algorithm = AlgorithmEnum.Twofish;
        }

        private void AreaDropData_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            System.Windows.Forms.OpenFileDialog dialog = new System.Windows.Forms.OpenFileDialog();
            dialog.Multiselect = true;
            dialog.Title = "Choose file";
            var result = dialog.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                AreaDropData.Visibility = Visibility.Collapsed;
                AreaDropData.AllowDrop = false;
                var path = new FilePath(Path.Combine(dialog.InitialDirectory, dialog.FileName));
                FilePathList.Add(path);
                path.RemoveCommand = new DelegateCommand(() =>
                {
                    FilePathList.Remove(path);
                    if (FilePathList.Count <= 0)
                    {
                        AreaDropData.Visibility = Visibility.Visible;
                        AreaDropData.AllowDrop = true;
                        if (parentGrid.AllowDrop)
                        {
                            parentGrid.AllowDrop = false;
                            parentGrid.Drop -= ParentGrid_Drop;
                        }
                    }
                }, true);
                if (!parentGrid.AllowDrop)
                {
                    parentGrid.AllowDrop = true;
                    parentGrid.Drop += ParentGrid_Drop;
                }
            }
        }

        private void AreaDropData_MouseEnter(object sender, MouseEventArgs e)
        {
            Mouse.OverrideCursor = Cursors.Hand;
        }

        private void AreaDropData_MouseLeave(object sender, MouseEventArgs e)
        {
            Mouse.OverrideCursor = Cursors.Arrow;
        }
    }
}
