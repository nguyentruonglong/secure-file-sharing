﻿using DemoAss1.Algorithms;
using DemoAss1.Common;
using DemoAss1.Models;
using DemoAss1.Services;
using InTheHand.Net.Sockets;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace DemoAss1.Views
{
    public partial class ProgressPage : Page, INotifyPropertyChanged
    {
        private Device _device;
        private string _outputFolder;
        private ObservableCollection<FilePath> _filePathList;
        private AlgorithmEnum _algorithm;
        private string _files;
        private long _totalSize;
        private string _tocryptTime;
        private string _sendReceiveTime;
        private string _algorithmName;
        private Stopwatch timer;
        private string _currFilePath;
        private string _md5Value;

        public ProgressPage()
        {
            InitializeComponent();
            this.DataContext = this;
            tbActionType.Text = "Receive from: ";
            tbTocryptTime.Text = "Decrypt time: ";
            tbSendReceiveTime.Text = "Receive time: ";
            _files = "0/1";
            _md5Value = string.Empty;
            WaitSenderDialog.Visibility = Visibility.Visible;
        }

        public ProgressPage(Device device, 
            ObservableCollection<FilePath> filePathList, AlgorithmEnum algorithm)
        {
            InitializeComponent();
            this.DataContext = this;
            Device = device;
            DeviceName.Text = Device.HostName;
            _filePathList = filePathList;
            _algorithm = algorithm;
            _algorithmName = algorithm.ToString();
            _files = "0/1";
            _md5Value = string.Empty;

            if (device == null)
                return;
            
        }

        public ProgressPage(string deveiceName, string outputFolder)
        {
            InitializeComponent();
            this.DataContext = this;
            DeviceName.Text = deveiceName;
            _outputFolder = outputFolder;
        }

        private void Sender_Error(object sender, EventArgs e)
        {
            MessageBox.Show("Occurs an attempt!", "Error");
            Application.Current.Dispatcher.BeginInvoke(
                System.Windows.Threading.DispatcherPriority.Normal, new Action(() =>
                {
                    if (this.NavigationService.CanGoBack)
                    {
                        this.NavigationService.GoBack();
                    }
                }));
            
        }

        private void Sender_Completed(object sender, EventArgs e)
        {
            
        }

        private void Sender_Progressing(object sender, EventArgs e)
        {
            
        }

        private void Sender_StartSend(object sender, EventArgs e)
        {
            
        }

        private void Sender_Connecting(object sender, EventArgs e)
        {
            Application.Current.Dispatcher.BeginInvoke(
                System.Windows.Threading.DispatcherPriority.Normal, new Action(()=>
            {
                ProgressText.Text = "Conneting...";
            }));
        }

        public Device Device
        {
            get
            {
                return _device;
            }
            set
            {
                _device = value;
                OnPropertyChanged("Device");
            }
        }
        public string AlgorithmName
        {
            get
            {
                return _algorithmName;
            }
            set
            {
                _algorithmName = value;
                OnPropertyChanged("AlgorithmName");
            }
        }
        public string Md5Value
        {
            get
            {
                return _md5Value;
            }
            set
            {
                _md5Value = value;
                OnPropertyChanged("Md5Value");
            }
        }

        public string Files
        {
            get
            {
                return _files;
            }
            set
            {
                _files = value;
                OnPropertyChanged("Files");
            }
        }

        public long TotalSize
        {
            get
            {
                return _totalSize;
            }
            set
            {
                _totalSize = value;
                OnPropertyChanged("TotalSize");
            }
        }

        public string TocryptTime
        {
            get
            {
                return _tocryptTime;
            }
            set
            {
                _tocryptTime = value;
                OnPropertyChanged("TocryptTime");
            }
        }

        public string SendReceiveTime
        {
            get
            {
                return _sendReceiveTime;
            }
            set
            {
                _sendReceiveTime = value;
                OnPropertyChanged("SendReceiveTime");
            }
        }

        public string CurrFilePath
        {
            get
            {
                return _currFilePath;
            }
            set
            {
                _currFilePath = value;
                OnPropertyChanged("CurrFilePath");
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(String property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            //Task.Run(() =>
            //{
            //    if(App.actionType == ActionType.Sender)
            //        Md5Value = Converter.FileToMd5(_filePathList[0].Path);
            //});

            Task.Run(() =>
            {
                var rsa = new RSA();
                if (App.actionType == ActionType.Sender)
                {
                    

                    Client client = new Client(Server.IP);

                    client.Completed += Client_Completed;
                    client.Error += Client_Error;
                    client.Progress += Client_Progress;
                    client.Starting += Client_Starting;

                    // Connect to server
                    if (!client.ConnectServer())
                    {
                        MessageBox.Show("Receiver can not found!");
                        return;
                    }
                    byte[] rsaPublicKey = client.AuthenServer();

                    long fileSize = new FileInfo(_filePathList[0].Path).Length;
                    Application.Current.Dispatcher.BeginInvoke(
                                System.Windows.Threading.DispatcherPriority.Normal, new Action(() =>
                                {
                                    TotalSize = fileSize;
                                }));
                    var encryptKey = EncryptFile(rsa, rsaPublicKey);

                    // Send encrypt key file
                    client.SendEncryptKey(encryptKey);

                    // Send file name
                    client.SendFileInfo(Path.GetFileName(_filePathList[0].Path), fileSize, AlgorithmName);
                    // send data file
                    client.SendFileData(_filePathList[0].Path);
                    client.StopService();

                }
                else if (App.actionType == ActionType.Receiver)
                {
                    // Receive File
                    App.server.StartService();

                    App.server.Completed += Server_Completed;
                    App.server.Error += Server_Error;
                    App.server.Progress += Server_Progress;
                    App.server.Starting += Server_Starting;

                    byte[] bytes = new byte[0];

                    string outFile = string.Empty;
                    byte[] encryptKey = null;
                    while (true)
                    {
                        var procol = App.server.ListenFromStream(ref bytes);
                        
                        if (procol == Protocol.AUTHENTICATION)
                        {
                            var clientName = Encoding.ASCII.GetString(bytes).Replace("\0", string.Empty);
                            ConfirmUser(clientName);
                        }
                        else if(procol == Protocol.ENCRYPT_KEY)
                        {
                            encryptKey = Converter.Trim(bytes);
                            App.server.ConfirmClientAction(Status.OK);
                        }
                        else if (procol == Protocol.SEND_DATA)
                        {
                            var fileInfo = (List<string>)Converter.ByteArrayToObject(bytes);
                            Application.Current.Dispatcher.BeginInvoke(
                                System.Windows.Threading.DispatcherPriority.Normal, new Action(() =>
                                {
                                    WaitSenderDialog.Visibility = Visibility.Collapsed;
                                    AlgorithmName = fileInfo[2];
                                    _algorithm = AlgorithmName == "DES" ? AlgorithmEnum.DES : AlgorithmEnum.Twofish;
                                    TotalSize = long.Parse(fileInfo[1]);
                                    CurrFilePath = fileInfo[0];
                                }));
                            var filename = fileInfo[0];
                            App.server.ConfirmClientAction(Status.OK);
                            outFile = App.server.ReadFileFromClient(filename, _outputFolder);
                            break;
                        }
                    }
                    

                    var rsaPrivateKey = File.ReadAllBytes(App.PrivateKeyFile);
                    DecryptFile(rsa, rsaPrivateKey, encryptKey, outFile);

                    App.server.StopService();
                }
                else if (App.actionType == ActionType.Encryptor)
                {
                    //EncryptFile(rsa);
                }
                else if (App.actionType == ActionType.Decryptor)
                {
                    //DecryptFile(rsa);
                }

                //reset action type
                App.actionType = ActionType.None;
            });
        }

        private void ConfirmUser(string clientName)
        {
            Application.Current.Dispatcher.BeginInvoke(
            System.Windows.Threading.DispatcherPriority.Normal, new Action(() =>
            {
                WaitSenderDialog.Visibility = Visibility.Collapsed;
                var result = MessageBox.Show("Do you want to receive file from " + clientName, "",
                MessageBoxButton.YesNo, MessageBoxImage.Question);

                if (result == MessageBoxResult.Yes)
                {
                    var browser = new System.Windows.Forms.FolderBrowserDialog();
                    browser.Description = "Choose folder to save";
                    browser.RootFolder = Environment.SpecialFolder.Desktop;
                    var result1 = browser.ShowDialog();
                    if (result1 == System.Windows.Forms.DialogResult.OK)
                    {
                        var rsaPublicKey = File.ReadAllBytes(App.PublicKeyFile);
                        App.server.ResponsePublicKey(rsaPublicKey);
                        _outputFolder = browser.SelectedPath;
                        WaitSenderDialog.Visibility = Visibility.Visible;
                        tbNotify.Text = "Waiting for sender encrypt some files...";
                        DeviceName.Text = clientName;
                        //this.NavigationService.Navigate(new ProgressPage(clientName, browser.SelectedPath));
                    }

                }
                else
                {
                    App.server.ConfirmClientAction(Status.ERROR);
                }

            }));
        }

        
        private byte[] EncryptFile(RSA rsa, byte[] rsaPublicKey)
        {
            byte[] encryptKey = null;
            // Encrypt File
            if (_algorithm == AlgorithmEnum.Twofish)
            {
                var twofish = new TwoFish();
                twofish.Completed += Twofish_Completed;
                twofish.Error += Twofish_Error;
                twofish.Progress += Twofish_Progress;
                twofish.Starting += Twofish_Starting;

                //string key = Guid.NewGuid().ToString().Replace("-", string.Empty);
                // Ramdom key for twofish
                var key = twofish.GenerateKey();
                // Encrypt key
                encryptKey = rsa.Execute(true, rsaPublicKey, key);
                // Encrypt file with two fish
                twofish.Execute(true, _filePathList[0].Path, key);
            }
            else
            {
                var des = new DES();
                des.Completed += des_Completed;
                des.Error += des_Error;
                des.Progress += des_Progress;
                des.Starting += des_Starting;

                //string key = Guid.NewGuid().ToString().Replace("-", string.Empty);
                // Ramdom key for twofish
                var key = des.GenerateKey();
                encryptKey = rsa.Execute(true, rsaPublicKey, key);
                des.Execute(true, _filePathList[0].Path, key);
            }
            return encryptKey;
        }

        private void DecryptFile(RSA rsa, byte[] rsaPrivateKey, byte[] encryptKey, string outFile)
        {
            // Decrypt File
            if (_algorithm == AlgorithmEnum.Twofish)
            {
                var twofish = new TwoFish();
                twofish.Completed += Twofish_Completed;
                twofish.Error += Twofish_Error;
                twofish.Progress += Twofish_Progress;
                twofish.Starting += Twofish_Starting;


                // Decrypt key
                var key = rsa.Execute(false, rsaPrivateKey, encryptKey);
                // Decrypt file with two fish
                twofish.Execute(false, outFile, key);
            }
            else
            {
                var des = new DES();
                des.Completed += des_Completed;
                des.Error += des_Error;
                des.Progress += des_Progress;
                des.Starting += des_Starting;

                var key = rsa.Execute(false, rsaPrivateKey, encryptKey);
                des.Execute(false, outFile, key);
            }
        }

        private void Server_Starting(object sender, EventArgs e)
        {
            Application.Current.Dispatcher.BeginInvoke(
                System.Windows.Threading.DispatcherPriority.Normal, new Action(() =>
                {
                    timer = Stopwatch.StartNew();
                    ProgressText.Text = "Starting receive file...";
                    ProgressBar.Value = 0;
                }));
        }

        private void Server_Progress(object sender, EventArgs e)
        {
            var args = e as ProgressEventArgs;
            Application.Current.Dispatcher.BeginInvoke(
                System.Windows.Threading.DispatcherPriority.Normal, new Action(() =>
                {
                    ProgressText.Text = "Receving...";
                    ProgressBar.Value = (int)(args.percent * 100 / _totalSize);

                    SendReceiveTime = ((int)(timer.Elapsed.TotalSeconds)).ToString() + " (s)";
                }));
        }

        private void Server_Error(object sender, EventArgs e)
        {
            Application.Current.Dispatcher.BeginInvoke(
                System.Windows.Threading.DispatcherPriority.Normal, new Action(() =>
                {
                    try
                    {
                        Application.Current.Dispatcher.BeginInvoke(
                        System.Windows.Threading.DispatcherPriority.Normal, new Action(() =>
                        {
                            timer.Stop();
                            SendReceiveTime = ((int)(timer.Elapsed.TotalSeconds)).ToString() + " (s)";
                            MessageBox.Show("Occurs a reveice file attempt!");
                        }));
                    }
                    catch (Exception ex)
                    {

                    }
                }));
        }

        private void Server_Completed(object sender, EventArgs e)
        {
            Application.Current.Dispatcher.BeginInvoke(
                System.Windows.Threading.DispatcherPriority.Normal, new Action(() =>
                {
                    ProgressText.Text = "A file was received!";
                    ProgressBar.Value = 100;
                    Files = "1/1";
                    timer.Stop();
                    SendReceiveTime = ((int)(timer.Elapsed.TotalSeconds)).ToString() + " (s)";
                }));
        }

        private void Client_Starting(object sender, System.EventArgs e)
        {
            Application.Current.Dispatcher.BeginInvoke(
                System.Windows.Threading.DispatcherPriority.Normal, new Action(() =>
                {
                    timer = Stopwatch.StartNew();
                    ProgressText.Text = "Starting send file...";
                    ProgressBar.Value = 0;
                }));
        }

        private void Client_Progress(object sender, System.EventArgs e)
        {
            var args = e as ProgressEventArgs;
            Application.Current.Dispatcher.BeginInvoke(
                System.Windows.Threading.DispatcherPriority.Normal, new Action(() =>
                {
                    ProgressText.Text = "Sending...";
                    ProgressBar.Value = args.percent;

                    SendReceiveTime = ((int)(timer.Elapsed.TotalSeconds)).ToString() + " (s)";
                }));
        }

        private void Client_Error(object sender, System.EventArgs e)
        {
            Application.Current.Dispatcher.BeginInvoke(
                System.Windows.Threading.DispatcherPriority.Normal, new Action(() =>
                {
                    try
                    {
                        Application.Current.Dispatcher.BeginInvoke(
                        System.Windows.Threading.DispatcherPriority.Normal, new Action(() =>
                        {
                            timer.Stop();
                            SendReceiveTime = ((int)(timer.Elapsed.TotalSeconds)).ToString() + " (s)";
                            MessageBox.Show("Occurs a send file attempt!");
                        }));
                    }
                    catch (Exception ex)
                    {

                    }
                }));
        }

        private void Client_Completed(object sender, System.EventArgs e)
        {
            Application.Current.Dispatcher.BeginInvoke(
                System.Windows.Threading.DispatcherPriority.Normal, new Action(() =>
                {
                    ProgressText.Text = "A file was sent!";
                    ProgressBar.Value = 100;
                    Files = "1/1";
                    timer.Stop();
                    SendReceiveTime = ((int)(timer.Elapsed.TotalSeconds)).ToString() + " (s)";
                }));
        }

        private void des_Starting(object sender, EventArgs e)
        {
            Application.Current.Dispatcher.BeginInvoke(
                System.Windows.Threading.DispatcherPriority.Normal, new Action(() =>
                {
                    timer = Stopwatch.StartNew();
                    ProgressText.Text = "Prepraring file...";
                    ProgressBar.Value = 0;
                }));
        }

        private void des_Progress(object sender, EventArgs e)
        {
            var args = e as ProgressEventArgs;
            Application.Current.Dispatcher.BeginInvoke(
                System.Windows.Threading.DispatcherPriority.Normal, new Action(() =>
                {
                    if (App.actionType == ActionType.Sender)
                    {

                        CurrFilePath = _filePathList[0].Path;
                        ProgressText.Text = "Encrypting...";
                    }
                    else if(App.actionType == ActionType.Receiver)
                        ProgressText.Text = "Decrypting...";

                    ProgressBar.Value = args.percent;
                    TocryptTime = ((int)(timer.Elapsed.TotalSeconds)).ToString() + " (s)";
                }));
        }

        private void des_Error(object sender, EventArgs e)
        {
            Application.Current.Dispatcher.BeginInvoke(
                System.Windows.Threading.DispatcherPriority.Normal, new Action(() =>
                {
                    try
                    {
                        Application.Current.Dispatcher.BeginInvoke(
                        System.Windows.Threading.DispatcherPriority.Normal, new Action(() =>
                        {
                            timer.Stop();
                            TocryptTime = ((int)(timer.Elapsed.TotalSeconds)).ToString() + " (s)";
                            MessageBox.Show("Occurs an des attempt!");
                        }));
                    }
                    catch (Exception ex) { }

                }));
        }

        private void des_Completed(object sender, EventArgs e)
        {
            Application.Current.Dispatcher.BeginInvoke(
                System.Windows.Threading.DispatcherPriority.Normal, new Action(() =>
                {
                    ProgressText.Text = "Completed!";
                    ProgressBar.Value = 100;
                    timer.Stop();
                    TocryptTime = ((int)(timer.Elapsed.TotalSeconds)).ToString() + " (s)";
                }));
        }

        private void Twofish_Starting(object sender, EventArgs e)
        {
            Application.Current.Dispatcher.BeginInvoke(
                System.Windows.Threading.DispatcherPriority.Normal, new Action(() =>
                {
                    timer = Stopwatch.StartNew();
                    ProgressText.Text = "Prepraring file...";
                    ProgressBar.Value = 0;
                }));
        }

        private void Twofish_Progress(object sender, EventArgs e)
        {
            var args = e as ProgressEventArgs;
            Application.Current.Dispatcher.BeginInvoke(
                System.Windows.Threading.DispatcherPriority.Normal, new Action(() =>
                {
                    if (App.actionType == ActionType.Sender)
                    {
                        ProgressText.Text = "Encrypting...";
                        CurrFilePath = _filePathList[0].Path;
                    }
                    else if(App.actionType == ActionType.Receiver)
                        ProgressText.Text = "Decrypting...";

                    
                    ProgressBar.Value = args.percent;

                    TocryptTime = ((int)(timer.Elapsed.TotalSeconds)).ToString() + " (s)";
                }));
        }

        private void Twofish_Error(object sender, EventArgs e)
        {
            try
            {
                Application.Current.Dispatcher.BeginInvoke(
                System.Windows.Threading.DispatcherPriority.Normal, new Action(() =>
                {
                    timer.Stop();
                    TocryptTime = ((int)(timer.Elapsed.TotalSeconds)).ToString() + " (s)";
                    MessageBox.Show("Occurs an twofish attempt!");
                }));
            }
            catch(Exception ex)
            {

            }
            
        }

        private void Twofish_Completed(object sender, EventArgs e)
        {
            Application.Current.Dispatcher.BeginInvoke(
                   System.Windows.Threading.DispatcherPriority.Normal, new Action(() =>
                   {
                       ProgressText.Text = "Completed!";
                       ProgressBar.Value = 100;
                       timer.Stop();
                       TocryptTime = ((int)(timer.Elapsed.TotalSeconds)).ToString() + " (s)";
                   }));
            
        }
    }
}
