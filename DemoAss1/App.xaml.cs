﻿using DemoAss1.Algorithms;
using DemoAss1.Services;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace DemoAss1
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static Task serverTask = null;
        public static Server server = new Server();
        public static ActionType actionType = ActionType.None;

        public static string PublicKeyFile = string.Empty;
        public static string PrivateKeyFile = string.Empty;
        public static RSA rsa = new RSA();

        public App()
        {
            PublicKeyFile = Path.Combine(Path.GetTempPath(), "PublicKeyFile.key");
            PrivateKeyFile = Path.Combine(Path.GetTempPath(), "PrivateKeyFile.key");
            rsa.GenKey();
        }
    }

    public enum ActionType
    {
        Sender,
        Receiver,
        Encryptor,
        Decryptor,
        None
    }
}
