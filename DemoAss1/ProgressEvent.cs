﻿using System;

namespace DemoAss1
{
    public class ProgressEvent
    {
        public event EventHandler Starting;
        protected void OnStarting()
        {
            Starting?.Invoke(this, EventArgs.Empty);
        }

        public event EventHandler Progress;
        protected void OnProgress(long value)
        {
            Progress?.Invoke(this, new ProgressEventArgs(value));
        }

        public event EventHandler Completed;
        protected void OnCompleted()
        {
            Completed?.Invoke(this, EventArgs.Empty);
        }

        public event EventHandler Error;
        protected void OnError()
        {
            Error?.Invoke(this, EventArgs.Empty);
        }
    }


    public class ProgressEventArgs : EventArgs
    {
        public long percent = 0;
        public ProgressEventArgs(long value)
        {
            percent = value;
        }
    }
}
