﻿using DemoAss1.Algorithms;
using DemoAss1.Services;
using DemoAss1.Views;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Threading;
using System.Threading.Tasks;
using System;

namespace DemoAss1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            this.MainFrame.Content = new HomePage();
            
            
        }


        private void TextBlock_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void TextBlock_MouseLeftButtonUp_1(object sender, MouseButtonEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

        private bool clicado = false;
        private Point oldPoint = new Point();
        private void TitleBar_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            clicado = true;
            oldPoint = e.GetPosition(this);
        }

        private void TitleBar_MouseMove(object sender, MouseEventArgs e)
        {
            if (clicado)
            {
                Point newPoint = e.GetPosition(this);
                Point delta = new Point(newPoint.X - oldPoint.X, newPoint.Y - oldPoint.Y);

                this.Top += delta.Y;
                this.Left += delta.X;
            }
        }

        private void TitleBar_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            clicado = false;
        }

        private void Grid_MouseEnter(object sender, MouseEventArgs e)
        {
            clicado = false;
        }

        private void btnExtend_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            lvPanel.Visibility = Visibility.Visible;
            btnExtend.Visibility = Visibility.Collapsed;
        }
        
        private void ScanDevice_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            App.actionType = ActionType.None;
            this.MainFrame.Navigate(new ScanDevicePage());
        }

        private void lvPanel_MouseLeave(object sender, MouseEventArgs e)
        {
            lvPanel.Visibility = Visibility.Collapsed;
            btnExtend.Visibility = Visibility.Visible;
        }
        

        private void Encrypt_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            App.actionType = ActionType.Encryptor;
            this.MainFrame.Navigate(new AddFilePage());
        }

        private void Decrypt_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            App.actionType = ActionType.Decryptor;
            this.MainFrame.Navigate(new AddFilePage());
        }

        private void Exit_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            App.actionType = ActionType.None;
            Application.Current.Shutdown();
        }

        private void Setting_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            App.actionType = ActionType.None;
            
        }

        private void About_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            App.actionType = ActionType.None;
            this.MainFrame.NavigationService.Navigate(new About());
        }

        private void TextBlock_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.MainFrame.NavigationService.Navigate(new HomePage());
        }

        private void Checksum_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.MainFrame.NavigationService.Navigate(new Checksum());
        }
    }
}
